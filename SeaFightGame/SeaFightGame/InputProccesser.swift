import Foundation

final class InputProccesser {
    // Пользователь выбирает клетку поля
    static func inputCell() -> (x: String, y: Int) {
        while true {
            print("Выберите значение по горизонтали (a/b/c/d):")
            let x = readLine()
            print("Выберите значение по вертикали (0/1/2/3):")
            let y = readLine()

            guard 
                let x = x,
                let y = y,
                !x.isEmpty,
                !y.isEmpty
            else {
                print("Введите корректные значения")
                continue
            }

            guard
                let y = Int(y),
                [0, 1, 2, 3].contains(y),
                ["a", "b", "c", "d"].contains(x)
            else {
                print("Введите корректные значения. По горизонтали - (a/b/c/d). По вертикали - (0/1/2/3).")
                continue
            }

            return (x, y)
        }
    }
}
