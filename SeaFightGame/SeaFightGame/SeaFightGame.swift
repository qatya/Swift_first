import Foundation

final class SeaFightGame {
    private let playerFiller = PlayerFiller()

    private var player1: Player?
    private var player2: Player?
    
    // Старт игры: триггерим метод создания игроков
    func start() {
        player1 = playerFiller.createPlayer()
        player2 = playerFiller.createPlayer()
        play()
    }

    // Механизм игры
    private func play() {
        guard
            var activePlayer = player1,
            var passivePlayer = player2
        else {
            return
        }

        var isGameFinished = false

        while !isGameFinished {
            print("Ходит \(activePlayer.name)")

            let cell = InputProccesser.inputCell()
            guard let line = passivePlayer.field[cell.x] else {
                continue
            }
            let attactedCell = line[cell.y]

            if attactedCell == false {
                print("Промах!")
                let temp = passivePlayer
                passivePlayer = activePlayer
                activePlayer = temp
            } else {
                print("Попадание!")
                passivePlayer.aliveCellCount -= 1
                passivePlayer.field[cell.x]?[cell.y] = false
                isGameFinished = passivePlayer.aliveCellCount == 0
            }
        }
        print("Игра окончена. Победил \(activePlayer.name)!")
    }
}
