import Foundation

//Создание и наполнение сущностей игроков
final class PlayerFiller {
    //Создание игрока
    func createPlayer() -> Player {
        let playerName = inputName()
        let gameField = createField()

        return Player(name: playerName, field: gameField)
    }

    // Ввод имени
    private func inputName() -> String {
        while true {
            print("Введите имя:")
            let name = readLine()

            guard
                let name = name,
                !name.isEmpty
            else {
                print("Имя не корректно")
                continue
            }

            return name
        }
    }

    // Создание игрового поля
    private func createField() -> [String: [Bool]] {
        var field = ["a": [false, false, false, false],
                     "b": [false, false, false, false],
                     "c": [false, false, false, false],
                     "d": [false, false, false, false]]

        let shipsDecks = [1, 1, 2]
        for decksCount in shipsDecks {
            print("\(decksCount)-палубный корабль")

            var previousSelectedCell: (x: String, y: Int)? = nil
            var count = 0

            while count < decksCount {
                if count > 0 {
                    print("Разместите следующую палубу")
                }
                let cell = InputProccesser.inputCell()

                guard field[cell.x]?[cell.y] == false else {
                    print("Эта клетка уже занята. Выберите другую")
                    continue
                }

                if count == 0 {
                    guard canPlaceShip(field: field, cell: cell) else {
                        print("Нельзя размещать корабль влотную к другому корабло")
                        continue
                    }
                } else {
                    let neighbourCells = getNeighbourCells(field: field, cell: cell)
                    guard neighbourCells.count == 1 else {
                        print("Нельзя размещать корабль влотную к другому корабло")
                        continue
                    }
                    guard
                        let previousSelectedCell = previousSelectedCell,
                        neighbourCells[0] == previousSelectedCell
                    else {
                        print("Нельзя размещать части одного корабля невлотную друг к другу")
                        continue
                    }
                }

                field[cell.x]?[cell.y] = true
                count += 1
                previousSelectedCell = cell
            }
        }

        return field
    }
    
    // Проверка можно ли разместить корабль в выбранной клетке
    private func canPlaceShip(field: [String: [Bool]], cell: (x: String, y: Int)) -> Bool {
        let letters = ["a", "b", "c", "d"]
        for dx in [-1, 0, 1] {
            for dy in [-1, 0, 1] {
                guard let letterIndex = letters.firstIndex(of: cell.x) else {
                    continue
                }
                let currentXIndex = letterIndex + dx
                guard [0, 1, 2, 3].contains(currentXIndex) else {
                    continue
                }

                let currentX = letters[currentXIndex]
                let currentY = cell.y + dy
                if
                    [0, 1, 2, 3].contains(currentY),
                    field[currentX]?[currentY] == true
                {
                    return false
                }
            }
        }

        return true
    }

    //Возвращает инфу по соседним клеткам (относительно выбранной)
    private func getNeighbourCells(field: [String: [Bool]], cell: (x: String, y: Int)) -> [(x: String, y: Int)] {
        var result: [(x: String, y: Int)] = []

        let letters = ["a", "b", "c", "d"]
        for dx in [-1, 0, 1] {
            for dy in [-1, 0, 1] {
                guard
                    dx != dy,
                    let letterIndex = letters.firstIndex(of: cell.x)
                else {
                    continue
                }
                let currentXIndex = letterIndex + dx
                guard [0, 1, 2, 3].contains(currentXIndex) else {
                    continue
                }

                let currentX = letters[currentXIndex]
                let currentY = cell.y + dy
                if
                    [0, 1, 2, 3].contains(currentY),
                    field[currentX]?[currentY] == true
                {
                    result.append((currentX, currentY))
                }
            }
        }

        return result
    }
}
