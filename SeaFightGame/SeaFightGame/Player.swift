import Foundation

// Структура игрока
struct Player {
    let name: String
    var field: [String: [Bool]]
    var aliveCellCount = 4
}
