import UIKit

// HW 2

// Задача 1
let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Задача 2 И Задача 3
var milkPrice: Double = 3
milkPrice = 4.20

// Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

// Дополнительное задание
if let milkBottleCount = milkBottleCount {
    profit = milkPrice * Double(milkBottleCount)
    print("Прибыль = \(profit)")
} else {
    print("Молока нет")
}

// Задача 5
var employeesList: [String] = []
employeesList.append(contentsOf: ["Петр", "Геннадий", "Иван", "Марфа", "Андрей"])

//Задача 6
var isEveryoneWorkHard: Bool = false
let workingHours = 20

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)

// HW 3

// Задание 1
func makeBuffer() -> (String) -> Void {
    var buffer = ""

    func save(input: String) {
        if input != "" {
            buffer += input
        } else {
            print(buffer)
            buffer = ""
        }
    }

    return save
}

var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")


// Задание 2
func checkPrimeNumber(_ input: Int) -> Bool {
    if input <= 1 {
        return false
    }
    if input == 2 {
        return true
    }
    if input % 2 == 0 {
        return false
    }
    let upperBound = Int(Double(input).squareRoot())
    for i in stride(from: 3, to: upperBound + 2, by: 2) {
        if input % i == 0 {
            return false
        }
    }
    
    return true
}

print(checkPrimeNumber(7)) // true
print(checkPrimeNumber(8)) // false
print(checkPrimeNumber(13)) // true
print(checkPrimeNumber(27))



// HW 4

struct Device {
    let name: String
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    static let iPhone14Pro = Device(name: "iPhone 14 Pro",
                                    screenSize: CGSize(width: 393, height: 852),
                                    screenDiagonal: 6.1,
                                    scaleFactor: 3)
    static let iPhoneXR = Device(name: "iPhone XR",
                                 screenSize: CGSize(width: 414, height: 896),
                                 screenDiagonal: 6.06,
                                 scaleFactor: 2)
    static let iPadPro = Device(name: "iPad Pro",
                                screenSize: CGSize(width: 834, height: 1194),
                                screenDiagonal: 11,
                                scaleFactor: 2)
    
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

print(Device.iPhone14Pro.physicalSize())
print(Device.iPhone14Pro)
print(Device.iPhoneXR.physicalSize())
print(Device.iPhoneXR)
print(Device.iPadPro.physicalSize())
print(Device.iPadPro)



// HW 5

// Задание 1

// ФИГУРА

class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }

    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

// ПРЯМОУГОЛЬНИК

class Rectangle: Shape {
    private let height: Double
    private let width: Double

    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }

    override func calculateArea() -> Double{
        let area = height * width
        return area
    }

    override func calculatePerimeter() -> Double{
        let perimeter = (height + width) * 2
        return perimeter
    }
}

// КРУГ

class Circle: Shape {
    private let radius: Double

    init(radius: Double) {
        self.radius = radius
    }

    override func calculateArea() -> Double{
        let area = pow(radius, 2) * Double.pi
        return area
    }

    override func calculatePerimeter() -> Double{
        let perimeter = radius * Double.pi * 2
        return perimeter
    }
}

// КВАДРАТ

class Square: Shape {
    private let side: Double

    init(side: Double) {
        self.side = side
    }

    override func calculateArea() -> Double{
        let area = pow(side, 2)
        return area
    }

    override func calculatePerimeter() -> Double{
        let perimeter = side * 4
        return perimeter
    }
}

// ПРОВЕРКА

let shapes: [Shape] = [
    Circle(radius:2),
    Square(side:3),
    Rectangle(height:2, width:4)
]
var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    let currentArea = shape.calculateArea()
    let currentPerimeter = shape.calculatePerimeter()

    print("\(shape): Area = \(currentArea), Perimeter = \(currentPerimeter)")

    area += currentArea
    perimeter += currentPerimeter
}

print("Area = \(area), Perimeter = \(perimeter)")






// Задание 2

func findIndexWithGenerics<T: Comparable>(ofValue valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

 

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

var stringToFind = "пес"
if let foundIndex = findIndexWithGenerics(ofValue: stringToFind, in: arrayOfString) {
    print("Индекс \(stringToFind): \(foundIndex)")
} else {
    print("Значение не найдено")
}

let arrayOfDouble = [3.5, 2.6, 1.7, 0.8]

var doubleToFind = 1.7
if let foundIndex = findIndexWithGenerics(ofValue: doubleToFind, in: arrayOfDouble) {
    print("Индекс \(doubleToFind): \(foundIndex)")
} else {
    print("Значение не найдено")
}

let arrayOfInt = [3, 2, 1, 0]

var intToFind = 3
if let foundIndex = findIndexWithGenerics(ofValue: intToFind, in: arrayOfInt) {
    print("Индекс \(intToFind): \(foundIndex)")
} else {
    print("Значение не найдено")
}



// HW 6

// Задание 1

struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }

    let grade: Grade
    let requiredSalary: Int
    let fullName: String
}

protocol CandidateFilter {
    func filter(candidates: [Candidate]) -> [Candidate]
}

class GradeFilter: CandidateFilter {
    private let grade: Candidate.Grade
    
    init(grade: Candidate.Grade) {
        self.grade = grade
    }

    func filter(candidates: [Candidate]) -> [Candidate] {
        var result: [Candidate] = []
        for candidate in candidates {
            if candidate.grade == grade {
                result.append(candidate)
            }
        }
        return result
    }
}

class SalaryFilter: CandidateFilter {
    private let salary: Int
    
    init(salary: Int) {
        self.salary = salary
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        var result: [Candidate] = []
        for candidate in candidates {
            if candidate.requiredSalary == salary {
                result.append(candidate)
            }
        }
        return result
    }
    
}

class NameFilter: CandidateFilter {
    private let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        var result: [Candidate] = []
        for candidate in candidates {
            if candidate.fullName == name {
                result.append(candidate)
            }
        }
        return result
    }
    
}


let candidates = [
    Candidate(grade: .junior, requiredSalary: 50000, fullName: "Alex Smith"),
    Candidate(grade: .senior, requiredSalary: 120000, fullName: "Maria Garcia"),
    Candidate(grade: .middle, requiredSalary: 100000, fullName: "Kate Johns"),
]

let gradeFilter = GradeFilter(grade: .senior)
let filteredByGrade = gradeFilter.filter(candidates: candidates)

let salaryFilter = SalaryFilter(salary: 100000)
let filteredBySalary = salaryFilter.filter(candidates: candidates)

let nameFilter = NameFilter(name: "Kate Johns")
let filteredByName = nameFilter.filter(candidates: candidates)

print(filteredByGrade)
print(filteredBySalary)
print(filteredByName)

// Задание 2

extension Candidate.Grade {
    var value: Int {
        switch self {
        case .junior:
            return 1
        case .middle:
            return 2
        case .senior:
            return 3
        }
    }
}

class MinimumGradeFilter: CandidateFilter {
    let minimumGrade: Candidate.Grade
    
    init(minimumGrade: Candidate.Grade) {
        self.minimumGrade = minimumGrade
    }
    
    func filter (candidates: [Candidate]) -> [Candidate] {
        var result: [Candidate] = []
        for candidate in candidates {
            if candidate.grade.value >= minimumGrade.value {
                result.append(candidate)
            }
        }
        return result
    }
}

let minimumGradeFilter = MinimumGradeFilter(minimumGrade: .middle)
let filteredCandidates = minimumGradeFilter.filter(candidates: candidates)

print(filteredCandidates)




// HW 7

class NameList {
    var dictionary: [Character:[String]] = [:]
    
    func addName(name: String) {
        let firstChar = Array(name)[0]
        
        if var names = dictionary[firstChar] {
            names.append(name)
            dictionary[firstChar] = names
        } else {
            dictionary[firstChar] = [name]
        }
    }
    
    func printNames() {
        let sortedKeys = Array(dictionary.keys).sorted(by: <)
        
        for key in sortedKeys {
            print(key)
            if let names = dictionary[key] {
                for name in names {
                    print(name)
                }
            }
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.printNames()
